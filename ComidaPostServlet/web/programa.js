class Pedido {

    static comprar() {
        //1. Tomar datos que ingreso el usuario, de los inputs
        //2. Crear un objeto de javascript con esos datos
        let comida = {};
        comida.precio = document.querySelector("#precio").value;
        comida.descripcion = document.querySelector("#descripcion").value;
        comida.combo = document.querySelector("#combo").value;
        
        console.log(comida);
        
        
        //3. Convertir el objeto de JS a JSON
         let comidaJson = JSON.stringify(comida);
         console.log(comidaJson);
        
        // 4. Enviar el JSON al servidor con FETCH        
        fetch("Entrega",
                {method: "POST",
                    body: comidaJson
                });
    }

    static main() {
        document.querySelector("#pedidoBtn").setAttribute("onclick", "Pedido.comprar();");
    }
    
    static ejemploDeObjeto(){
        let comida = {};
        comida.descripcion = "5 kilos de papas. 2 de zanahoria. 1 Sandia";
        comida.precio = 200;
        comida.combo = "Combo 1";
        console.log(comida);
    }
    
    
}

Pedido.main();